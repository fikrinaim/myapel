import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavController, ToastController } from '@ionic/angular';
import { UpdatepasswordService } from 'src/app/services/updatepassword.service';
import { TranslateService } from '@ngx-translate/core';
import { Formerror } from '../updatepassword/formerror';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.page.html',
  styleUrls: ['./resetpassword.page.scss'],
})
export class ResetpasswordPage extends Formerror implements OnInit {
  tukarKatalaluanForm: FormGroup;
  EmailSignUp: any;


  constructor(
    private formBuilder: FormBuilder,
    private navController: NavController,
    private resetPassword: UpdatepasswordService,
    private toastController: ToastController,
    public translate : TranslateService
  ) {
    super();
   }

  ngOnInit() {
    this.tukarKatalaluanForm = this.formBuilder.group(
      {
       
        nokp: ['',[Validators.required,Validators.maxLength(14), Validators.minLength(12)]]

      }
    )
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: '',
      message: '<u>Notis</u><br>Sila semak e-mel anda untuk mendapatkan kod pengesahan.<br><br><u>Notices</u><br>Please check your e-mail for verification code.',
      position: 'middle',
      buttons: [
        {
          text: 'Tutup/Close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }

  async presentToastWithOptions2() {
    const toast = await this.toastController.create({
      header: '',
      message: '<u>Notis</u><br>Tiada rekod ditemui.<br><br><u>Notice</u><br>No record found.',
      position: 'middle',
      buttons: [
        {
          text: 'Tutup/Close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }
  doReset(){
    console.log('Klik doReset')
    let postData = {
     
      nokp: this.tukarKatalaluanForm.controls['nokp'].value

    }
    this.resetPassword.postResetPassword(postData).then(
      res => {
        this.EmailSignUp = res
        if (this.EmailSignUp.length > 0) {
          console.log('run else function')
          console.log(postData)
          this.presentToastWithOptions()
          this.navController.navigateRoot('/confirmreset/' +postData.nokp)

        } 
        // else if (this.EmailSignUp.length < 0) {
        //   console.log('run else if function')
        //   console.log(postData)
          
        // }
        else {
          console.log('Kau salah input')
          this.presentToastWithOptions2()
          
      }

      }


    )

  }

  chgL(lang: string) {
    this.translate.use(lang)
  }
  }


