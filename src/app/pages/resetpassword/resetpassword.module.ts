import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ResetpasswordPage } from './resetpassword.page';
import { TranslateService, TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: ResetpasswordPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    TranslateModule
  ],
  declarations: [ResetpasswordPage]
})
export class ResetpasswordPageModule {}
