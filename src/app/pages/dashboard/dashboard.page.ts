import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private storage: Storage,
    private router: Router
  ) {  }

  ngOnInit() {
    // let id = this.route.snapshot.paramMap.get('nokp')
    // console.log(id , "<<<<<no kp")
    return new Promise((resolve, reject) => {
      this.storage.get('USER_INFO').then((value) => {
          resolve(value);
          
      });
      
  });
  }


  logoutUser(){
    this.storage.remove('USER_INFO').then(() => {
      this.router.navigate(['/login']);
      this.authService.authState.next(false);
    });
  }


}
