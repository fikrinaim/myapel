import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup;
  passwordType: string = 'password';
  passwordShown: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
  ) { }
  public togglePassword(){
    if(this.passwordShown){
      this.passwordShown = false;
      this.passwordType = 'password';
    }else{
      this.passwordShown = true;
      this.passwordType = 'text';
    }

  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group(
      {
        
        password: ['', [Validators.required]],
        confirmpassword: ['', [Validators.required]],
        nric: ['', [Validators.required, Validators.maxLength(12), Validators.minLength(12)]],
        fullname: ['', [Validators.required]],
        dob: ['', [Validators.required]],
        gender: ['', [Validators.required]],
        race: ['', [Validators.required]],
        income: ['', [Validators.required]],
        lvlacademic: ['', [Validators.required]],
        add1: ['', [Validators.required]],
        add2: ['', [Validators.required]],
        add3: ['', [Validators.required]],
        postcode: ['', [Validators.required]],
        city: ['', [Validators.required]],
        state: ['', [Validators.required]],
        nohp: ['', [Validators.required]]

      }
    )
  }

}
