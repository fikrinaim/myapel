import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApplicationService } from 'src/app/services/application.service';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-applicationresult',
  templateUrl: './applicationresult.page.html',
  styleUrls: ['./applicationresult.page.scss'],
})
export class ApplicationresultPage implements OnInit {
  levelArray : any[] = new Array<any>();
  ApplicationResult : any[] = new Array<any>();

  level =false;
  result=false;

  norecords=false;

  user:any;
  
  applicationResultForm :FormGroup;

  constructor(
    private formBuilder : FormBuilder,
    private applicationService:ApplicationService,
    private loadingController : LoadingController,
    private storage:Storage
  ) { }

  ngOnInit() {
    this.applicationResultForm=this.formBuilder.group({
      level: ''
    })

    this.applicationService.getLevelFromApi().then(
      response => {
       this.levelArray = response
      }
    )

    return new Promise((resolve, reject) => {
      this.storage.get('USER_INFO').then((value) => {
          resolve(value);
          
          this.user=value;

      });
      
  });

  }

  async changeLevel(){
    
    let userSearch ={
      nokp: this.user.nric,
      peringkat: this.applicationResultForm.controls['level'].value
    }

    const loading = await this.loadingController.create({
      message: 'Please Wait...',
    });
    await loading.present();

  
    this.applicationService.getApplicationResult(userSearch.nokp,userSearch.peringkat).then(
      res => {
       this.ApplicationResult = res
console.log (res)
       loading.dismiss()

       if ( this.ApplicationResult.length > 0) {

        this.result=true;
        this.norecords=false;
       
        
     }else{
      this.norecords=true;
      this.result=false;
     }
      }

      
    )
    
  }

}
