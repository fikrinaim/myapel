import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegisteremailService } from 'src/app/services/registeremail.service';
import { HTTP } from '@ionic-native/http/ngx';
import { NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  registerForm: FormGroup;
  EmailSignUp: any;


  constructor(
    private formBuilder: FormBuilder,
    private registerEmailService: RegisteremailService,
    public http: HTTP,
    private navController: NavController,
    private toastController: ToastController,

  ) { }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: '',
      message: '<u>Pendaftaran Tidak Berjaya</u><br>E-mel atau No.MyKad telah didaftarkan.<br><br><u>Registration Failed</u><br>E-mail or MyKad No. already registered.',
      position: 'middle',
      buttons: [
        {
          text: 'Tutup/Close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }

  async presentToastWithOptions2() {
    const toast = await this.toastController.create({
      header: '',
      message: '<u>Pendaftaran Tidak Berjaya</u><br>Bagi pemohon baharu, sila daftar terlebih dahulu di https://www2.mqa.gov.my/myapela<br><br><u>Registration Failed</u><br>If you are a new candidate, please register at https://www2.mqa.gov.my/myapela',
      position: 'middle',
      buttons: [
        {
          text: 'Tutup/Close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group(
      {

        email: ['', [Validators.required, Validators.email]],
        NoKP:['',[Validators.required,Validators.maxLength(14), Validators.minLength(12)]]

      }
    )
  }
  doRegister() {


    let postData = {

      email: this.registerForm.controls['email'].value,
      NoKP: this.registerForm.controls['NoKP'].value

    }

    this.registerEmailService.postRegistrationEmail(postData).then(
      res => {
        console.log(res)
        this.EmailSignUp = res
        if (this.EmailSignUp === 1) {
          console.log('Email already registered')
          this.presentToastWithOptions()

        } else if (this.EmailSignUp === 2) {
          console.log('run else if function')
          console.log(postData)
          this.navController.navigateRoot('/verify/' + postData.email)
          
        }
        else {
          console.log('run else function')
          console.log(postData)
          this.presentToastWithOptions2()
          
      }

      }


    )

  }

}
