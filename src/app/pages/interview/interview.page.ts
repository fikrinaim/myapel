import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ApplicationService } from 'src/app/services/application.service';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-interview',
  templateUrl: './interview.page.html',
  styleUrls: ['./interview.page.scss'],
})
export class InterviewPage implements OnInit {

  levelArray : any[] = new Array<any>();
  Interview : any[] = new Array<any>();

  level =false;
  result=false;

  norecords=false;
  user:any;
  
  interviewResultForm :FormGroup;

  constructor(
    private formBuilder : FormBuilder,
    private applicationService:ApplicationService,
    private loadingController:LoadingController,
    private storage:Storage
  ) { }

  ngOnInit() {

   /* this.interviewResultForm=this.formBuilder.group({
      level: ''
    })

    this.applicationService.getLevelFromApi().then(
      response => {
       this.levelArray = response
      }
    )*/

    return new Promise((resolve, reject) => {
      this.storage.get('USER_INFO').then((value) => {
          resolve(value);
          
          this.user=value;

          console.log(value)

          
          this.applicationService.getInterview(this.user.nric,7).then(
            res => {
             this.Interview = res
      
           
      
             if ( this.Interview.length > 0) {
      
              this.result=true;
              this.norecords=false;
             
              
           }else{
            this.norecords=true;
            this.result=false;
           }
            }
      
            
          )
      });
      
  });

   
  }

 /*async changeLevel(){

    let userSearch ={
      nokp: this.user.nric,
      peringkat: this.interviewResultForm.controls['level'].value
    }

    const loading = await this.loadingController.create({
      message: 'Please Wait...',
    });
    await loading.present();
  
    this.applicationService.getInterview(userSearch.nokp,userSearch.peringkat).then(
      res => {
       this.Interview = res

       loading.dismiss()

       if ( this.Interview.length > 0) {

        this.result=true;
        this.norecords=false;
       
        
     }else{
      this.norecords=true;
      this.result=false;
     }
      }

      
    )

    
   }*/

}
