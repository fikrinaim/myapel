import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApplicationService } from 'src/app/services/application.service';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-aptitudetest',
  templateUrl: './aptitudetest.page.html',
  styleUrls: ['./aptitudetest.page.scss'],
})
export class AptitudetestPage implements OnInit {

 
  levelArray : any[] = new Array<any>();
  Aptitude : any[] = new Array<any>();




  level =false;
  result=false;

  norecords=false;
  user : any;
  ppa:any;
  emelppa:any;

  aptitudeForm :FormGroup;

  constructor(
    private formBuilder : FormBuilder,
    private applicationService:ApplicationService,
    private loadingController : LoadingController,
    private storage : Storage
  ) { }

  ngOnInit() {
    this.aptitudeForm=this.formBuilder.group({
      level: ''
    })

    this.applicationService.getLevelFromApi().then(
      response => {
       this.levelArray = response
      }
    )

    return new Promise((resolve, reject) => {
      this.storage.get('USER_INFO').then((value) => {
          resolve(value);
          
          this.user=value;

      });
      
  });
  }

  async changeLevel(){
    let userSearch ={
      nokp: this.user.nric,
      peringkat: this.aptitudeForm.controls['level'].value
    }

    console.log(userSearch.peringkat)

    const loading = await this.loadingController.create({
      message: 'Please Wait...',
    });
    await loading.present();

    this.applicationService.getAptitude(userSearch.nokp,userSearch.peringkat).then(
      res => {
       this.Aptitude = res

       loading.dismiss()
       console.log(this.Aptitude[0].data)     


      if ( this.Aptitude[0].data == 0) {
     

     this.result=false;
    this.norecords=true;
        this.ppa=this.Aptitude[0].pusatpenilaian
        this.emelppa=this.Aptitude[0].emelppa
        
      }else if ( this.Aptitude[0].data == 1) {
   
       this.norecords=false;
       this.result=true;
       this.ppa=this.Aptitude[0].pusatpenilaian
       this.emelppa=this.Aptitude[0].emelppa
      } 
      }

      
    )

    
  }

}
