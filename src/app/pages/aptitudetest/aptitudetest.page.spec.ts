import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AptitudetestPage } from './aptitudetest.page';

describe('AptitudetestPage', () => {
  let component: AptitudetestPage;
  let fixture: ComponentFixture<AptitudetestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AptitudetestPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AptitudetestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
