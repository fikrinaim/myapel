import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VerifyAccountService } from 'src/app/services/verifyaccount.service';
import { HTTP } from '@ionic-native/http/ngx';
import { NavController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.page.html',
  styleUrls: ['./verify.page.scss'],
})
export class VerifyPage implements OnInit {
  registerForm: FormGroup;
  EmailSignUp: any;

  constructor(
    private formBuilder: FormBuilder,
    private verifyAccount: VerifyAccountService,
    public http: HTTP,
    private navController: NavController,
    private toastController: ToastController,
    public translate: TranslateService
  ) { }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: 'Register Failed',
      message: 'Wrong input.Please key in the correct input.',
      position: 'middle',
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group(
      {
        
        email: ['', [Validators.required, Validators.email]],
        verificationcode: ['', [Validators.required]],
        NoKP: ['',[Validators.required,Validators.maxLength(14), Validators.minLength(12)]]

      }
    )
  
    
  }

  doVerify() {


    let postData = {

      email: this.registerForm.controls['email'].value,
      NoKP: this.registerForm.controls['NoKP'].value,
      verificationcode:this.registerForm.controls['verificationcode'].value

    }

    this.verifyAccount.postVerifyAccount(postData).then(
      res => {
        this.EmailSignUp = res
        if (this.EmailSignUp.length > 0) {
          console.log('run else function')
          console.log(postData)
          this.navController.navigateRoot('/updatepassword/' +postData.NoKP)

        } 
        // else if (this.EmailSignUp.length < 0) {
        //   console.log('run else if function')
        //   console.log(postData)
          
        // }
        else {
          console.log('Kau salah input')
          this.presentToastWithOptions()
          
      }

      }


    )

  }

}
