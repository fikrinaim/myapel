import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NavController, ToastController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  registerForm: FormGroup;
  passwordType: string = 'password';
  passwordShown: boolean = false;
  LoginResult: any[] = new Array<any>();
  showPassword = false;

  constructor(
    private formBuilder: FormBuilder,
    public translate: TranslateService,
    public navController: NavController,
    private userService: UserService,
    private toastController: ToastController,
    private authService: AuthenticationService,
    private storage: Storage,
    private router: Router
  ) { }

async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: '',
      message: '<u>Daftar masuk tidak berjaya</u><br>Sila masukkan No. MyKad/Passport atau kata laluan yang betul.<br><br><u>Login Failed</u><br>Please key in the correct MyKad No./Passport or Password',
      position: 'middle',
      buttons: [
        {
          text: 'Tutup/Close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }
  
  togglePassword() {
    if(this.showPassword == true) {
      this.showPassword = false
    }else {
      this.showPassword = true
    }
  }

  doLogin() {


    let postData = {
      nokp: this.registerForm.controls['nric'].value,
      password: this.registerForm.controls['password'].value,
    }
     
    this.userService.login(postData)
    .then(data => {

      this.LoginResult = data
      

      if (this.LoginResult.length > 0) {
       this.navController.navigateRoot('/dashboard/' + postData.nokp)
        var userInfo = {
          nric: this.LoginResult[0].nokp,
          email: this.LoginResult[0].email
        };

        this.storage.set('USER_INFO', userInfo).then((response) => {
          this.router.navigateByUrl('/dashboard/'+userInfo.nric);
          this.authService.authState.next(true);
        });
      
      } else {
       console.log('Login Failed')
       this.presentToastWithOptions()
       
      }
      this.registerForm.reset();

    }); 

  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group(
      {

        password: ['', [Validators.required]],
        nric: ['', [Validators.required, Validators.maxLength(14), Validators.minLength(12)]]

      }
    )
  }

  chgL(lang: string) {
    this.translate.use(lang)
  }
}
