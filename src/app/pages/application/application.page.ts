import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { LoadingController,  AlertController, ModalController } from '@ionic/angular';
import { ApplicationService } from 'src/app/services/application.service';
import { HttpClient } from '@angular/common/http';
import {HTTP} from '@ionic-native/http/ngx';
import { ModalPage } from 'src/app/modal/modal.page';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-application',
  templateUrl: './application.page.html',
  styleUrls: ['./application.page.scss'],
})
export class ApplicationPage implements OnInit {

  levelArray : any[] = new Array<any>();
  ppaArray : any[] = new Array<any>();
  bidangArray : any[] = new Array<any>();
  bidangByPPA: any[] = new Array<any>();
  
  user :any;

  T3T4Choosen =false;
  T6Choosen =false;
  T7Choosen =false;

  ppabybidang =false;



  applicationForm :FormGroup;



  constructor(
    private formBuilder:FormBuilder,
    private applicationService:ApplicationService,
    public loadingController:LoadingController,
    public http: HTTP,
    public alertController: AlertController,
    public modalController: ModalController,
    private storage: Storage
  
  ) { }

  ngOnInit() {

    this.applicationService.getLevelFromApi().then(
      response => {
       this.levelArray = response
      }
    )

    this.applicationForm=this.formBuilder.group({
      level: ['',Validators.required],
      testcenter: ['',Validators.required],
      field:''
    })

    return new Promise((resolve, reject) => {
      this.storage.get('USER_INFO').then((value) => {
          resolve(value);
          
          this.user=value;

      });
  });
  
  }

  async changePeringkat(value){

    const loading = await this.loadingController.create({
      message: 'Please Wait...',
    });
    await loading.present();

    this.applicationService.getPPAFromApi(value).then(
      res => {
       this.ppaArray = res
       loading.dismiss()
      }
    )

   

    if (value=='3' || value == '4'){
      this.T3T4Choosen = true
      this.T6Choosen = false
      this.T7Choosen = false
      this.ppabybidang =false
    } 
    else if (value=='6'){
      this.T6Choosen = true
      this.T3T4Choosen = false
      this.T7Choosen = false
      this.ppabybidang =false
    }
    else if (value=='7'){

this.applicationService.getBidangList().then(
      data => {
       this.bidangArray = data
      
      }

    )
      this.T7Choosen = true
      this.T3T4Choosen = false
      this.T6Choosen = false
    }
    }

  async changeBidang(idbidang){
    const loading = await this.loadingController.create({
      message: 'Please Wait...',
    });
    await loading.present();

    this.applicationService.getPPAbyBidang(idbidang).then(
      bidang => {
       this.bidangByPPA = bidang
       this.ppabybidang = true
       loading.dismiss()
      }
    )
   
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalPage
    });

    return await modal.present();
  }



  async apply(){

   
    let postData ={

      peringkat:this.applicationForm.controls['level'].value,
      pusatpeperiksaan:this.applicationForm.controls['testcenter'].value,
      bidang:this.applicationForm.controls['field'].value,
      nokp: this.user.nric
      
    }

    this.applicationService.postApplication(postData).then(
      res=>{
     
        this.presentModal()

      },
      error=>{
      
     
      }
      
    )
    
    }

  }


