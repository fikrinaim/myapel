import { AbstractControl } from '@angular/forms';


export class Formerror{
    passwordPattern:    string = '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}';
    numberPattern:    string = '[^\d{12}$]';
    MatchPassword(ac: AbstractControl) {
        let password = ac.get('password').value; // to get value in input tag
        let confirmPassword = ac.get('repassword').value; // to get value in input tag
         if(password != confirmPassword) {
             ac.get('repassword').setErrors( {MatchPassword: true} )
         } else {
             return null
         }
     }
}
