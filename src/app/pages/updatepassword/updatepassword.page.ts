import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HTTP } from '@ionic-native/http/ngx';
import { NavController, ToastController } from '@ionic/angular';
import { UpdatepasswordService } from 'src/app/services/updatepassword.service';
import { ActivatedRoute } from '@angular/router';
import { Formerror } from './formerror';
import {  TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-updatepassword',
  templateUrl: './updatepassword.page.html',
  styleUrls: ['./updatepassword.page.scss'],
})
export class UpdatepasswordPage extends Formerror implements OnInit {
  updatePasswordForm:FormGroup;
  nokp:string;
  passwordUpdate:any;
  showPassword = false;

  constructor(
    private formBuilder: FormBuilder,
    private updatePassword:UpdatepasswordService,
    public http: HTTP,
    private navController: NavController,
    private toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    public translate : TranslateService
    
  ) {
    super();
    this.nokp = this.activatedRoute.snapshot.paramMap.get("id");
  }

  togglePassword() {
    if(this.showPassword == true) {
      this.showPassword = false
    }else {
      this.showPassword = true
    }
  }
  ngOnInit() {
    this.updatePasswordForm=this.formBuilder.group({
      password: ['',[Validators.pattern(this.passwordPattern)]],
      repassword: [""]
    },
		{
		  validator: this.MatchPassword
    })
    console.log(this.nokp)
  }
  doUpdatePassword() {
    let postData = {

      password: this.updatePasswordForm.controls['password'].value,
      nokp: this.nokp

    }
    
    this.updatePassword.postUpdatePassword(postData).then(
      res => {
        this.passwordUpdate = res
        if (this.passwordUpdate.length > 0) {
          console.log('run else function')
          console.log(postData)
          this.navController.navigateRoot('/login')

        } 
        // else if (this.EmailSignUp.length < 0) {
        //   console.log('run else if function')
        //   console.log(postData)
          
        // }
        else {
          console.log('Kau salah input')
          
      }

      }


    )
  }
  chgL(lang: string) {
    this.translate.use(lang)
  }
}
