import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { VerifyAccountService } from 'src/app/services/verifyaccount.service';
import { NavController, ToastController } from '@ionic/angular';
import { UpdatepasswordService } from 'src/app/services/updatepassword.service';
import { Formerror } from '../updatepassword/formerror';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-confirmreset',
  templateUrl: './confirmreset.page.html',
  styleUrls: ['./confirmreset.page.scss'],
})
export class ConfirmresetPage extends Formerror implements OnInit {
  resetPassForm: FormGroup;
  reset: any;  
  showPassword = false;

  constructor(
    private formBuilder: FormBuilder,
    private confirmResetPass: UpdatepasswordService,
    private navController: NavController,
    private toastController: ToastController,
    public translate : TranslateService
  ) { 
    super();
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: '',
      message: '<u>Kemas Kini Gagal</u>Sila masukkan input yang betul.<br><br><u>Update Failed</u><br>Please key in the correct input',
      position: 'middle',
      buttons: [
        {
          text: 'Tutup/Close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }

  async presentToastWithOptionsProceed() {
    const toast = await this.toastController.create({
      header: '',
      message: '<u>Kemas Kini Berjaya</u><br>Sila cuba semula.<br><br><u>Update Successfull</u><br>Try to login with the new password.',
      position: 'middle',
      buttons: [
        {
          text: 'Tutup/Close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }

  ngOnInit() {
    this.resetPassForm = this.formBuilder.group(
      {
        
        verificationcode: ['', [Validators.required]],
        NoKP: ['',[Validators.required,Validators.maxLength(14), Validators.minLength(12)]],
        password: ['',[Validators.pattern(this.passwordPattern)]]

      }
    )
  }
  togglePassword() {
    if(this.showPassword == true) {
      this.showPassword = false
    }else {
      this.showPassword = true
    }
  }
  doVerify() {


    let postData = {
      
      NoKP: this.resetPassForm.controls['NoKP'].value,
      verificationcode:this.resetPassForm.controls['verificationcode'].value,
      password:this.resetPassForm.controls['password'].value

    }

    this.confirmResetPass.postConfirmReset(postData).then(
      res => {
        this.reset = res
        if (this.reset.length > 0) {
          console.log('run else function')
          console.log(postData)
          this.presentToastWithOptionsProceed()
          this.navController.navigateRoot('/login')

        } 
        // else if (this.EmailSignUp.length < 0) {
        //   console.log('run else if function')
        //   console.log(postData)
          
        // }
        else {
          this.presentToastWithOptions()
          console.log('Kau salah input')
          
      }

      }


    )

  }

}
