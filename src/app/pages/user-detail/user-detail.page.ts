import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApplicationService} from 'src/app/services/application.service';
import { Storage } from '@ionic/storage';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.page.html',
  styleUrls: ['./user-detail.page.scss'],
})
export class UserDetailPage implements OnInit {

  race =false;
  userdetailForm: FormGroup;
  passwordType: string = 'password';
  passwordShown: boolean = false;
  userdetail: any[]=new Array<any>();
  test: string;
  user: any;

  constructor(
    private formBuilder: FormBuilder,
    private _application:ApplicationService,
    private storage: Storage,
    private loadingController : LoadingController
  ) {
    this.initForm();
  }

  public togglePassword(){
    if(this.passwordShown){
      this.passwordShown = false;
      this.passwordType = 'password';
    }else{
      this.passwordShown = true;
      this.passwordType = 'text';
    }

  }

  async ngOnInit() {

    const loading = await this.loadingController.create({
      message: 'Please Wait...',
    });
    await loading.present();
    
    return new Promise((resolve, reject) => {
      this.storage.get('USER_INFO').then((value) => {
          resolve(value);
          
          loading.dismiss()
          
          this.user=value;
          console.log(this.user)
          this._application.getUserDetail(this.user.nric).then(
            response => {
              
      
            //  this.userdetailForm.patchValue({
            //    fullname: response[0].username
              
            //  })
      
              console.log(response[0])
             this.userdetail = response;
             console.log(response[0].nokp)
            }
          )

      });

    });
    
    

    
  }

  initForm(){
    this.userdetailForm = this.formBuilder.group(
      {
        
        nokp: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.maxLength(12), Validators.minLength(12)]],
        fullname: ['', [Validators.required]],
        dob: ['', [Validators.required]],
        gender: ['', [Validators.required]],
        race: ['', [Validators.required]],
        add1: ['', [Validators.required]],
        add2: ['', [Validators.required]],
        add3: ['', [Validators.required]],
        postcode: ['', [Validators.required]],
        city: ['', [Validators.required]],
        state: ['', [Validators.required]],
        nohp: ['', [Validators.required]],
        emel: ['', [Validators.required]]

      }
    )
  }


}
