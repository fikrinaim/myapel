import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {HTTP} from '@ionic-native/http/ngx';

declare var cordova : any;
let headers = {'Content-Type':'application/x-www-form-urlencoded'};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private httpClient: HTTP
  ) { }

  // getUserToLogin(nokp:string,password:string): Promise<any>{
  //   return this.httpClient.get('https://www2.mqa.gov.my/rest/apelapp/login/nokp/'+ nokp+ '/password/' + password, {}, headers)
  //   .then((data)) => {
  //     return JSON.parse(data.data);
    // })
    // .catch((error)) => {
    //   console.log(error);
    // });

    login(postData):Promise<any>{
     
      return new Promise((resolve, reject) => {
        cordova.plugin.http.post('https://www2.mqa.gov.my/rest/apelapp/login/', postData, { "Content-Type": 'application/x-www-form-urlencoded' },
        function(response) {
          try {
            // console.log(response);
  
            //if success login
            var data = JSON.parse(response.data);
            //console.log(data);
  
              //try ni :D
              var user = data.status;
  
              resolve(data);
  
            }catch(e) {
            console.error('JSON parsing error');
          }
        },
          function(fail){
            //reject("ERROR HERE");
            console.log(fail);
            console.error(fail);
          }
        )
      })
    }
 }

