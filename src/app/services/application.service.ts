import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {HTTP} from '@ionic-native/http/ngx';
import { HttpClient } from '@angular/common/http';

declare var cordova : any;
let headers = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

@Injectable({
  providedIn: 'root'
})


export class ApplicationService {
  login: any;
  register(newUser: { name: any; email: any; password: any; dob: any; }) {
    throw new Error("Method not implemented.");
  }

  constructor(
    private httpClient: HttpClient,
    private httpNgx : HTTP
  ) { }

 
getLevelFromApi(): Promise<any>{

  return this.httpNgx.get('https://www2.mqa.gov.my/rest/apelapp/apellevel/id/null', {}, headers)
  .then((data) => {
      return JSON.parse(data.data);
  })
  .catch((error) => {
      console.log(error);
  });
}

getPPAFromApi(value): Promise<any>{
  return this.httpNgx.get('https://www2.mqa.gov.my/rest/apelapp/ppa/id/'+ value, {}, headers)
  .then((data) => {
    return JSON.parse(data.data);
})
.catch((error) => {
    console.log(error);
});
}

getBidangList(): Promise<any>{

  return this.httpNgx.get('https://www2.mqa.gov.my/rest/apelapp/bidanglist/id/null', {}, headers)
  .then((data) => {
    return JSON.parse(data.data);
})
.catch((error) => {
    console.log(error);
});
}

getPPAbyBidang(idbidang): Promise<any>{
  return this.httpNgx.get('https://www2.mqa.gov.my/rest/apelapp/bidang/id/'+ idbidang, {}, headers)
  .then((data) => {
    return JSON.parse(data.data);
})
.catch((error) => {
    console.log(error);
});
}

getInterview(nokp:string,peringkat:number): Promise<any>{
   return this.httpNgx.get('https://www2.mqa.gov.my/rest/apelapp/portfolio/nokp/'+ nokp+ '/peringkat/' + peringkat, {}, headers)
   .then((data) => {
    return JSON.parse(data.data);
})
.catch((error) => {
    console.log(error);
});
}


getAptitude(nokp:string,peringkat:number): Promise<any>{
  return this.httpNgx.get('https://www2.mqa.gov.my/rest/apelapp/apttest/nokp/'+ nokp+ '/peringkat/' + peringkat , {}, headers)
  .then((data) => {
    return JSON.parse(data.data);
})
.catch((error) => {
    console.log(error);
});
}

getApplicationResult(nokp:string,peringkat:number): Promise<any>{
  return this.httpNgx.get('https://www2.mqa.gov.my/rest/apelapp/result/nokp/'+ nokp+ '/peringkat/' + peringkat , {}, headers)
  .then((data) => {
    return JSON.parse(data.data);
})
.catch((error) => {
    console.log(error);
});
}

postApplication(postData):Promise<any>{

 
  return new Promise((resolve, reject) => {
    cordova.plugin.http.post('https://www2.mqa.gov.my/rest/apelapp/permohonan', postData, { "Content-Type": 'application/x-www-form-urlencoded' },
    function(response) {
      try {
        // console.log(response);

        //if success login
        var data = JSON.parse(response.data);
        //console.log(data);

          //try ni :D
          var user = data.status;

          resolve(data);

        }catch(e) {
        console.error('JSON parsing error');
      }
    },
      function(fail){
        //reject("ERROR HERE");
        console.log(fail);
        console.error(fail);
      }
    )
  })
}

getUserDetail(nokp:string): Promise<any>{
  return this.httpNgx.get('https://www2.mqa.gov.my/rest/apelapp/pemohon/nokp/'+ nokp, {}, headers)
  .then((data) => {
    return JSON.parse(data.data);
})
.catch((error) => {
    console.log(error);
});
}

}
