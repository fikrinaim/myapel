import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { HttpClientModule } from '@angular/common/http';






declare var cordova: any;
let headers = {
  'Content-Type': 'application/x-www-form-urlencoded'
};
@Injectable({
  providedIn: 'root'
})
export class RegisteremailService {

  constructor(
    private httpClient: HTTP
  ) { }

  postRegistrationEmail(postData):Promise<any>{

 
    return new Promise((resolve, reject) => {
      cordova.plugin.http.post('https://www2.mqa.gov.my/rest/apelapp/emeldaftar', postData, { "Content-Type": 'application/x-www-form-urlencoded' },
      function(response) {
        try {
          // console.log(response);
  
          //if success login
          var data = JSON.parse(response.data);
          //console.log(data);
  
            //try ni :D
            var user = data.status;
  
            resolve(data);
  
          }catch(e) {
          console.error('JSON parsing error');
        }
      },
        function(fail){
          //reject("ERROR HERE");
          console.log(fail);
          console.error(fail);
        }
      )
    })
  }
}
