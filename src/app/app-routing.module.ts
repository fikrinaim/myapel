import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomePageModule) },
  { path: 'signup', loadChildren: './pages/signup/signup.module#SignupPageModule' },
  { path: 'user-detail', loadChildren: './pages/user-detail/user-detail.module#UserDetailPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'contact-us', loadChildren: './pages/contact-us/contact-us.module#ContactUsPageModule' },
  { path: 'application', loadChildren: './pages/application/application.module#ApplicationPageModule' },
  { path: 'dashboard', loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule'},
  { path: 'verify', loadChildren: './pages/verify/verify.module#VerifyPageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'interview', loadChildren: './pages/interview/interview.module#InterviewPageModule' },
  { path: 'aptitudetest', loadChildren: './pages/aptitudetest/aptitudetest.module#AptitudetestPageModule' },
  { path: 'applicationresult', loadChildren: './pages/applicationresult/applicationresult.module#ApplicationresultPageModule' },
  { path: 'modal', loadChildren: './modal/modal.module#ModalPageModule' },
  { path: 'updatepassword', loadChildren: './pages/updatepassword/updatepassword.module#UpdatepasswordPageModule' },
  { path: 'updatepassword/:id', loadChildren: './pages/updatepassword/updatepassword.module#UpdatepasswordPageModule' },
  { path: 'resetpassword', loadChildren: './pages/resetpassword/resetpassword.module#ResetpasswordPageModule' },
  { path: 'confirmreset/:id', loadChildren: './pages/confirmreset/confirmreset.module#ConfirmresetPageModule' },


  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
