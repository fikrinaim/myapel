import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {

user :any;

  constructor(navParams: NavParams,
    public modalCtrl : ModalController,
    public navController : NavController,
    private storage :Storage,
    private router: Router
    ) {
    // componentProps can also be accessed at construction time using NavParams
    console.log(navParams.get('level'));
  }

  ngOnInit() {
    return new Promise((resolve, reject) => {
      this.storage.get('USER_INFO').then((value) => {
          resolve(value);
          
          this.user=value;

      });
  });
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true,

    });

    // this.navController.navigateRoot('/dashboard')
    this.router.navigateByUrl('/dashboard/'+this.user.nric);
  }

}
